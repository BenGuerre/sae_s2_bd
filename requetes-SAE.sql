-- SAE2.04 1
-- Nom:GUERRE  , Prenom:Benjamin 

-- +------------------+--
-- * Question 1 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations sur les boîtes qui contiennent une pièce de couleur Very Light Orange

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+----------------------------+-------+----------+---------+
-- | numboite | nomboite                   | annee | nbpieces | idtheme |
-- +----------+----------------------------+-------+----------+---------+
-- | 5823-1   | The Good Fairy's Bedroom   | 2000  | 33       | 319     |
-- | etc...
-- = 
SELECT numboite, nomboite, annee, nbpieces, idtheme
FROM BOITE NATURAL JOIN CONTENU NATURAL JOIN CONTENIR NATURAL JOIN PIECE NATURAL JOIN COULEUR
WHERE nomcoul = "Very Light Orange"
order by numboite;


-- +------------------+--
-- * Question 2 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations sur les contenus qui sont présents à la fois dans INCLURE et dans CONTENIR.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +--------+---------+-------------+
-- | idcont | version | numboite    |
-- +--------+---------+-------------+
-- | 87     | 1       | 5001925-1   |
-- | etc...
-- = Reponse question 2.
select idcont, version, numboite from CONTENU where idcont in (select idcont from INCLURE) and idcont in (select idcont from CONTENIR);


-- +------------------+--
-- * Question 3 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations des pièces qui ne sont utilisées que dans des boîtes du thème Jungle.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +--------------+----------------------------------------------------------------------------------------------------------------------------------------------------+-------+
-- | numpiece     | nompiece                                                                                                                                           | idcat |
-- +--------------+----------------------------------------------------------------------------------------------------------------------------------------------------+-------+
-- | 2454px6      | Brick 1 x 2 x 5 with Jungle Print                                                                                                                  | 2     |
-- | etc...
-- = Reponse question 3.
SELECT numpiece, nompiece, idcat 
FROM PIECE natural join CONTENIR NATURAL JOIN CONTENU NATURAL JOIN BOITE NATURAL JOIN THEME
WHERE nomtheme = "Jungle" and numpiece not in (select numpiece from PIECE NATURAL JOIN CONTENIR NATURAL JOIN CONTENU NATURAL JOIN BOITE NATURAL JOIN THEME WHERE nomtheme <> "Jungle")
ORDER BY numpiece;


-- +------------------+--
-- * Question 4 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations sur la ou les pièces utilisées en plus grande quantité dans une seule boîte.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+-------------+-------+
-- | numpiece | nompiece    | idcat |
-- +----------+-------------+-------+
-- | 3024     | Plate 1 x 1 | 14    |
-- +----------+-------------+-------+
-- = Reponse question 4.
select numpiece, nompiece, idcat
from PIECE natural join CONTENIR
where quantitep >= ALL(select quantitep from PIECE natural join CONTENIR);
-- +------------------+--
-- * Question 5 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  L'année où pour la première fois une boîte du thème Technic est apparue.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------+
-- | annee |
-- +-------+
-- | 1973  |
-- +-------+
-- = Reponse question 5.
select MIN(annee) 
FROM BOITE NATURAL JOIN THEME
WHERE nomtheme = "Technic";


-- +------------------+--
-- * Question 6 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Pour chaque année, on voudrait connaitre le nombre de couleurs différentes utilisées dans les boîtes crées cette année là.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------+--------+
-- | annee | nbcoul |
-- +-------+--------+
-- | 1950  | 10     |
-- | etc...
-- = Reponse question 6.
SELECT annee, count(distinct idcoul) nbcoul
FROM CONTENIR NATURAL JOIN CONTENU NATURAL JOIN BOITE
GROUP BY annee;


-- +------------------+--
-- * Question 7 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  les informations des pièces qui ne sont contenues dans aucune boîte.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------+
-- | numpiece       | nompiece                                                                                                                                                                                        | idcat |
-- +----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------+
-- | 10             | Baseplate 24 x 32                                                                                                                                                                               | 1     |
-- | etc...
-- = Reponse question 7.
SELECT numpiece, nompiece, idcat 
FROM PIECE 
WHERE numpiece not in (SELECT numpiece FROM CONTENIR);


-- +------------------+--
-- * Question 8 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  la requête qui associe à chaque identifiant de thème le nom de son thème principal.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +---------+----------------------------------+
-- | idtheme | nomtheme                         |
-- +---------+----------------------------------+
-- | 1       | Technic                          |
-- | etc...
-- = Reponse question 8.
select t1.idtheme, if(t2.idtheme is null , t1.nomtheme , if(t3.nomtheme is null , t2.nomtheme, t3.nomtheme)) nomtheme
from THEME t1 left join THEME t2
on t1.idtheme_1 = t2.idtheme
left join THEME t3 on t3.idtheme = t2.idtheme_1
order by t1.idtheme;


-- +------------------+--
-- * Question 9 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  la requête qui donne les informations des boîtes du thème Jungle qui utilisent plus de 100 pièces différentes. Deux pièces sont identiques si elles ont la même référence et la même couleur.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+--------------------------+-------+----------+---------+----------+
-- | numboite | nomboite                 | annee | nbpieces | idtheme | nbpieces |
-- +----------+--------------------------+-------+----------+---------+----------+
-- | 5976-1   | River Expedition         | 1999  | 318      | 299     | 173      |
-- | etc...
-- = Reponse question 9.
select numboite, nomboite, annee, nbpieces, idtheme, count(numpiece) nbPieces
from BOITE natural join CONTENU natural join CONTENIR natural join THEME
where nomtheme = "Jungle"
GROUP by numboite
having nbPieces > 100;


-- +-------------------+--
-- * Question 10 :     --
-- +-------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  la requête qui permet de retrouver le contenu complet d'un CONTENU incluant son propre contenu ainsi que celui des boîtes qui le composent.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +--------+-----------------+--------+-----------+---------------+
-- | idcont | numpiece        | idcoul | quantitep | en_supplement |
-- +--------+-----------------+--------+-----------+---------------+
-- | 1      | 48379c01        | 72     | 1         | f             |
-- | etc...
-- = Reponse question 10.
select idcont, numpiece, idcoul, quantitep, en_supplement
from BOITE natural join CONTENU natural join INCLURE natural join CONTENIR;
--la

-- +-------------------+--
-- * VUES :             --
-- +-------------------+--
--1
create or replace view PIECE_INUTILISEES as
    select numpiece, nompiece, idcat 
    from PIECE natural left join CONTENIR
    where idcont is null;

--2
create or replace view THEME_PRINCIPAL(idtheme,nomthprinc) as  
    select t1.idtheme, if(t2.idtheme is null , t1.nomtheme , if(t3.nomtheme is null , t2.nomtheme, t3.nomtheme)) nomtheme
    from THEME t1 left join THEME t2
    on t1.idtheme_1 = t2.idtheme
    left join THEME t3 on t3.idtheme = t2.idtheme_1
    order by t1.idtheme;

--3
create or replace view JUNGLE100(numboite, nomboite, annee, nbpieces, idtheme, nbpieces) as 
    select numboite, nomboite, annee, nbpieces, idtheme, count(numpiece) nbPieces
    from BOITE natural join CONTENU natural join CONTENIR natural join THEME
    where nomtheme = "Jungle"
    GROUP by numboite
    having nbPieces > 100;

--4
create or replace view CONTENU_COMPLET(idcont,numpiece,idcoul,quantite,en_supplement) as
    select idcont, numpiece, idcoul, quantitep, en_supplement 
    from CONTENIR natural join CONTENU natural join INCLURE natural join BOITE;


-- +-------------------+--
-- * LES GRAPHIQUES :   --
-- +-------------------+--

SELECT nomcoul, SUM(quantitep) somme
from BOITE natural join CONTENU natural join CONTENIR natural join COULEUR
WHERE nomboite = ?
group by idcoul
order by somme DESC
LIMIT 10;

SELECT nomtheme, COUNT(numboite)
from BOITE natural join THEME natural join THEME_PRINCIPAL
where nomthprinc = ? and nomtheme <> ?
group by idtheme;