--Ajoute de la nouvelle boite
INSERT INTO BOITE(numboite, nomboite, annee, nbpieces, idtheme) 
VALUES( "60161-1", "IUT'O", 2022, 6, 50);

--Création d'un contenu pour la boite
INSERT INTO CONTENU(idcont, version, numboite)
VALUES (18709, 1, "60161-1");
--Ajoute 3 bear de couleur sand blue
INSERT INTO CONTENIR(idcont, numpiece, idcoul, quantitep, en_supplement) VALUES
(18709, "bear", 379, 3, "f");

--Ajout 1 Crab de couleur Light Salmon
INSERT INTO CONTENIR(idcont, numpiece, idcoul, quantitep, en_supplement)
VALUES (18709, "33121", 100, 1, "f");

--Ajout 1 Kragle de couleur Unknown
INSERT INTO CONTENIR(idcont, numpiece, idcoul, quantitep, en_supplement) VALUES 
    (18709, "kragle", -1, 1, "f"),
    (18709, "kragle", -1, 1, "t");
--Ajout de 1 Kragle de couleur Unknown en supplément


