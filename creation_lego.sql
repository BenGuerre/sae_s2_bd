CREATE DATABASE IF NOT EXISTS `LEGO` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `LEGO`;

CREATE TABLE `CATEGORIE` (
  `idcat` int,
  `nomcat` varchar(50),
  PRIMARY KEY (`idcat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PIECE` (
  `numpiece` varchar(20),
  `nompiece` varchar(250),
  `idcat` int,
  PRIMARY KEY (`numpiece`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `COULEUR` (
  `idcoul` int,
  `nomcoul` varchar(200),
  `rgb` varchar(6),
  `transparent` char(1),
  PRIMARY KEY (`idcoul`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CONTENU` (
  `idcont` int,
  `version` int,
  `numboite` varchar(20),
  PRIMARY KEY (`idcont`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CONTENIR` (
  `idcont` int,
  `numpiece` varchar(20),
  `idcoul` int,
  `quantitep` int,
  `en_supplement` char(1),
  PRIMARY KEY (`idcont`, `numpiece`, `idcoul`, `en_supplement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `INCLURE` (
  `idcont` int,
  `numboite` varchar(20),
  `quantiteb` int,
  PRIMARY KEY (`idcont`, `numboite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `BOITE` (
  `numboite` varchar(20),
  `nomboite` varchar(100),
  `annee` int,
  `nbpieces` int,
  `idtheme` int,
  PRIMARY KEY (`numboite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `THEME` (
  `idtheme` int,
  `nomtheme` varchar(50),
  `idtheme_1` int,
  PRIMARY KEY (`idtheme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `PIECE` ADD FOREIGN KEY (`idcat`) REFERENCES `CATEGORIE` (`idcat`);
ALTER TABLE `CONTENU` ADD FOREIGN KEY (`numboite`) REFERENCES `BOITE` (`numboite`);
ALTER TABLE `CONTENIR` ADD FOREIGN KEY (`idcoul`) REFERENCES `COULEUR` (`idcoul`);
ALTER TABLE `CONTENIR` ADD FOREIGN KEY (`numpiece`) REFERENCES `PIECE` (`numpiece`);
ALTER TABLE `CONTENIR` ADD FOREIGN KEY (`idcont`) REFERENCES `CONTENU` (`idcont`);
ALTER TABLE `INCLURE` ADD FOREIGN KEY (`numboite`) REFERENCES `BOITE` (`numboite`);
ALTER TABLE `INCLURE` ADD FOREIGN KEY (`idcont`) REFERENCES `CONTENU` (`idcont`);
ALTER TABLE `BOITE` ADD FOREIGN KEY (`idtheme`) REFERENCES `THEME` (`idtheme`);
ALTER TABLE `THEME` ADD FOREIGN KEY (`idtheme_1`) REFERENCES `THEME` (`idtheme`);