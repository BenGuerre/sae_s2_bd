--Les pieces qui sont dans la boite Takutanuva éditée en 2003(numéro 10201-1)
select numpiece, nompiece, idcat, nomcat, idcoul, nomcoul, quantitep
from PIECE natural join CONTENIR natural join CONTENU natural join BOITE natural join CATEGORIE natural join COULEUR
where numboite = "10201-1";

--Les boites incluse dans cette boite
select numboite, nomboite
from INCLURE natural right join BOITE where idcont = (select idcont from CONTENU  where numboite = "10201-1" );

--La liste des pièces (nom, quantité, catégorie, couleur) qui sont en quantité supérieure à 10 dans ces boîtes incluses
select nompiece, quantitep, idcat, nomcat, idcoul, nomcoul
from BOITE natural join CONTENU natural join CONTENIR natural join  CATEGORIE natural join COULEUR natural join PIECE
where numboite = "8593-1" or numboite = "8596-1" 
having quantitep > 10;

--liste des pièces supplémentaire des sous-boites
select  numpiece, nompiece, idcat, nomcat, idcoul, nomcoul, quantitep, en_supplement
from CONTENIR natural join CONTENU natural join BOITE natural join PIECE natural join COULEUR natural join CATEGORIE
where numboite = "8593-1" or numboite = "8596-1" 
having en_supplement = "t";




SELECT nomcoul, SUM(quantitep) somme
from BOITE natural join CONTENU natural join CONTENIR natural join COULEUR
WHERE nomboite = "Spider-Man Super Jumper"
group by idcoul
order by somme DESC
LIMIT 10;

SELECT t2.nomtheme, count(numboite) nombre_de_boite
from BOITE natural join`THEME` t1  join `THEME` t2 on t1.idtheme_1 = t2.idtheme 
WHERE t1.nomtheme = "Technic"
GROUP BY t2.idtheme;
